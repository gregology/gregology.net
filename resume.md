---
layout: page
redirect_from:
  - /cv/
  - /cv
  - /résumé/
  - /résumé
comments: False
licence: Creative Commons
title: Résumé
---

## Work History

### Senior Data Scientist @ Shopify
Jul 2019 to present, Ottawa Canada

* Data modelling, reporting, experimenting, & machine learning
* Mainly working in [Python](https://pypi.org/user/gregology/) & [Ruby](https://rubygems.org/profiles/gregology) with [Spark](https://spark.apache.org/docs/latest/api/python/), [PrestoDB](https://prestodb.io), & [Rails](https://rubyonrails.org/)

### Developer & CEO @ Memair
Nov 2018 to Jul 2019, Ottawa Canada

* Empowering humans by using business analytics & data science techniques on their own data
* Learning how to business
* Working with [Rails](https://rubygems.org/profiles/gregology), [Python](https://pypi.org/user/gregology/), [GraphQL](https://memair.com/graphiql), [React](https://memair.com/player), [Flutter](https://flutter.dev/), & [Javascript](https://www.npmjs.com/~gregology) on Google Cloud, Heroku, & physical servers

### Sailing Sabbatical
Sep 2017 to Sep 2018, Canada, USA, & The Bahamas

* Sailing [SV Catsaway](https://SVCatsaway.com) from Kingston, Ontario to the Bahamas and back
* Producing a [YouTube series](https://YouTube.com/SVCatsaway) documenting our adventure
* Hacking on a few (mostly boat related) [projects](/packages)

### Data Engineer @ Shopify
Sep 2013 to Sep 2017, Ottawa Canada

* Data modeling, reporting, experimenting, & machine learning
* Mainly working in [Python](https://pypi.org/user/gregology/) & [Ruby](https://rubygems.org/profiles/gregology) with [Spark](https://spark.apache.org/docs/latest/api/python/), [PrestoDB](https://prestodb.io), & [Rails](https://rubyonrails.org/)

### Acroyoga Instructor @ Upward Dog Yoga Centre
Jul 2014 to present, Ottawa Canada

* Teaching Acroyoga as part of [SmileyOm](https://smileyom.com)

### Data Specialist @ Amnesty International
Mar 2012 to Sep 2013, Sydney Australia & Ottawa Canada

* Project management for supporter database
* Mainly working with [Rails](http://rubyonrails.org/) & SQL

### Communications Specialist @ Australian Army
Jul 2004 to Nov 2012, Australia

* Peace making tour in Afghanistan
* Peace keeping tour in Solomon Islands

## Education

### Bachelor's degree @ Murdoch University
Feb 2008 to Mar 2011, by correspondence

* Terrorism, counter-terrorism, and security

## Security Clearance

Government of Canada Security Clearance<br>
**Level:** Enhanced Reliability<br>
**File Number:** 96200871-0002397761<br>
**Expiry Date:** April 4, 2029<br>
