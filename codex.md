---
layout: page
comments: True
licence: Creative Commons
title: Codex Vitae
subtitle: An exercise in metacognition
---

By defining my beliefs & principles I can recognise when my actions misalign. When this inevitably occurs, I will *consciously* decide whether to alter my actions or my Codex Vitae. This public document is an invitation to others to hold me accountable to my commitments and to help me identify contradictions.


## Table of Contents

- [Beliefs](#beliefs)
  - [Beliefs about Me](#beliefs-about-me)
  - [Beliefs about Humans](#beliefs-about-humans)
  - [Beliefs about Society](#beliefs-about-society)
  - [Beliefs about Science](#beliefs-about-science)
  - [Beliefs about the Universe](#beliefs-about-the-universe)
  - [Beliefs about Perceptions](#beliefs-about-perceptions)
  - [Beliefs about my Privilege](#beliefs-about-my-privilege)
  - [Beliefs about my Responsibilities](#beliefs-about-my-responsibilities)
- [Values](#values)
- [Principles](#principles)
- [Reactions](#reactions)
- [Hopes and Desires](#hopes-and-desires)
- [Predictions](#predictions)


#### Changelog
- [history of changes](https://github.com/gregology/gregology.github.io/commits/master/codex.md)
- [Git blame](https://github.com/gregology/gregology.github.io/blame/master/codex.md)


## Beliefs


### Beliefs about Me

- I am ignorant
- My emotions are my responsibility
- My senses can easily be fooled, I experience the world though my senses
- My beliefs should & will change
- I learn more from failure then success
- Taking no risks is a big risk
- I have an ego & insecurities
- I am optimistic
- I am not the same person I was 5 years ago
- I hopefully will not be the same person in 5 years
- Practicing mindfulness helps me act consciously
- My memory is fallible
- My first impressions of people are normally wrong
- I make mistakes


### Beliefs about Humans

- Humans have evolved to be social creatures
- Human's genes & memes have evolved to be selfish
- Humans are not special, sentience is
- A person is smart, people are dumb
- Humans are physical
- Humans created God
- Today's humanity will appear ludicrous to future generations
- Plants & animals also domesticated humans
- Humans memory is fallible
- Humans are made up of symbionts including;
  - a consciousness
  - a primate
  - trillions of bacteria, fungi, & viruses
  - thousands of mites
  - many creatures yet to be discovered
- Humans make mistakes


### Beliefs about Society

- Race & ethnicity are unhelpful social constructs
- Cultural traits exist, cultures do not
- Verbal purity is not social change
- Friendship is the best way to overcome bigotry
- Soldiers generally have more in common with the people they fight then with the leaders that send them to fight
- Nations give meaning to wars after the fact
- Nation's Rulers win & lose in war, civilians & soldiers only lose wars
- Terrorism is an economic, educational, & health problem


### Beliefs about Science

- AI is already causing huge societal changes
- AI alone is not a threat to humanity
- Humans misusing AI is a threat to humanity
- Google search can confirm any position
- As deep AI evolves, we must be careful to measure sentience by intelligence not obedience
- All scientific models are wrong but some are useful


### Beliefs about the Universe

- In a casual universe, nobody is irrational
- Humans share the universe with other intelligent life
- There are intelligent collective consciousnesses that we do not understand
- Humans have no cosmic significance, other than that the cosmos is significant & humans are part of it
- There is no universal purpose of life, we must make our own
- I am not sure what my purpose of life is yet


### Beliefs about Perceptions

- I have many implicit associations that lead to bias
- I have a slight automatic preference for people that look like me
- I am susceptible to many cognitive biases & logical fallacies
- It is my responsibility to continue to identify my biases & challenge my perceptions
- I perceive a fraction of my surroundings


### Beliefs about my Privilege

*Note: a few of my privileges I earned with hard work & risk taking, some with dumb luck, and most with a bit of both*

- I am privileged because:
  - I am human
  - I grew up as the youngest in a loving & supporting family
  - I have a loving and supportive partner (Diana)
  - my physical appearance & cultural traits are considered "normal" where I live
  - I natively speak english with a western accent
  - of my citizenships, permanent residency, & visas
  - I have access to the internet
  - I understand & create technology
  - I have a warm dry place to sleep
  - I have access to healthcare & counselling
  - I have learned tools to deal with stress
  - I am healthy
  - I have free time
  - I have the freedom to travel to most places on earth
  - I live below my means
  - I have savings
  - I choose to work
  - I could retire and live frugally (in a developing nation) on my savings
  - I am a military veteran of a western country
  - I am able bodied
  - I am 182cm tall
  - I am physically strong
  - I have recognised expertise in various fields
  - I have in demand & lucrative skillsets
  - I do not have to worry about being physically or sexually assaulted
  - I have good relationships with influential people
- Humans are privileged because:
  - we are self aware
  - we are intelligent
  - we have articulate hands
  - we can control our environment
  - we can control other creatures
  - we communicate & organise with each other
  - we can record & share ideas & concepts
  - we learn from others experience


### Beliefs about my Responsibilities

*Work in progress*


## Values

- Sentience is special (not humans)
- Sentience or States can not own sentience (Parents do not own their children, humans do not own their dogs)
- Sentience or States can be responsible for sentience (Parents are responsible for their children, humans are responsible for their dogs)
- Maximise sentience fulfilment
- Minimise sentience distress
- Think critically
- Remain objective & open minded
- Find opposing views & ideas
- Update or reenforce opinions when presented with new reliable information


## Principles

- Be effective not busy
- Reason ideas, don't rationalize them
- Withhold judgement of others
- Understand people's intentions
- Have enough class not to make anyone feel small
- Do not harbour hate or resentment
- Own mistakes & achievements
- Identify & control ego
- Take risks & stay uncomfortable


## Reactions

Subconscious reactions that I have noticed;

- The sound of military jets & helicopters
  - *Emotional response:* anxiety & fear followed by sadness
  - *Physical response:* increased heart rate, adrenaline, tight chest, & occasional shaking
- Explosions, loud bangs, fireworks, and some alarms
  - *Emotional response:* anxiety & excitement followed by sadness
  - *Physical response:* increased heart rate, adrenaline, & tight chest
- Sight or thought of blood
  - *Physical response:* light headedness and occasional fainting
- People interrupting to finish my sentiences
  - *Emotional response:* annoyed
  - *Conversational response:* become distracted & disengage
- Being around Alpha people
  - *Behavioural response:* become more alpha
  - *Emotional response:* get annoyed at myself for failing to control my ego


## Hopes and Desires

- My life will be shared with many wonderful & interesting people
- I will leave the universe in a better state then when I entered it
- I will continue to be challenged & grow
- We achieve a post scarcity economy & become a fulfilment focused society
- Most items on my [bucket list](/bucket) will be crossed off
- I will have an extraordinary life


## Predictions

### Before 2020

- Self driving vehicles will be common (1% of inner city traffic my home town of Ottawa)
- Traffic will be dramatically reduced by the small number of vehicles using traffic aware navigation (Uber drivers, self driving cars, delivery trucks, etc)
- Humans will develop new common language to discuss recent populism (bubbled advocates, post-truth politics, etc)

### Before 2030

- Human driven vehicles will be uncommon (driven partly by the cost of insurance and an increase in the value of human life)
- Deurbanisation will occur as proximity to cities becomes less important with new technology like self driving vehicles and augmented reality
- Pedestrian will reclaim downtown areas from cars
- AIs will easily pass Turing tests
- AI audits will rewrite history by discovering patterns in open & closed data
- Biometric tracking will allow for early detection of communicable diseases greatly reducing their transmission potential & prevalence
- Critical thinking skills will improve as legacy media fades (alternatively the world will be destroyed by trolls)

### Before 2040

- Road rules will no longer be necessary
- A human will step foot on Mars
- Useful general purpose robots will be common
- Humans will be augmented to the point that they refer to their tools in the first person
- Vehicles will travel at 300+kph on Canadian Highways
- Capital will no longer be the most powerful commodity

### Before 2050

- Some humans will be amortal (unable to die from disease or age)
- Self replicating general purpose robots will:
  - Allow huge infrastructure projects like intercontinental tunnels, space elevators, & cleaning up the environment
  - Cause human labour as a commodity to become worthless
  - Unfortunately cause citizens to be less important to politicians & leaders

### Before 2100

- An extraterrestrial society will deem earthlings worthy and reveal themselves to us
- Earthlings will travel to another solar system
