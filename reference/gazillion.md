---
title: Gazillion
layout: page
disqus: yep
redirect_from:
  - /GregologicalStudies/gazillion
  - /GregologicalStudies/Gazillion
  - /gregologicalstudies/gazillion
  - /gregologicalstudies/Gazillion
  - /gazillion
  - /Gazillion
---
### A Gazillion

How much is a Gazillion? Lets first break down the word Gazillion. Etymology of `llion`
From million, from Latin mille, thousand, plus augmentative suffix -ion or -on (literally big thousand)

  * Million has 6 zeros
  * Billion has 9 zeros
  * Trillion has 12 zeros
  * Quadrillion has 15 zeros
  * Quintillion has 18 zeros
  * Sextillion has 21 zeros
  * Septillion has 24 zeros
  * Octillion has 27 zeros
  * Nonillion has 30 zeros
  * Decillion has 33 zeros
  * Undecillion has 36 zeros
  * Duodecillion has 39 zeros
  * Tredecillion has 42 zeros
  * Quattuordecillion has 45 zeros
  * Quindecillion has 48 zeros
  * Sexdecillion has 51 zeros
  * Septendecillion has 54 zeros
  * Octodecillion has 57 zeros
  * Novemdecillion has 60 zeros
  * Vigintillion has 63 zeros
  * Googol has 100 zeros.
  * Centillion has 303 zeros (except in Britain, where it has 600 zeros)
  * Googolplex has a googol of zeros

### Etymology of Gaz

Gazzen, from Latin `earthly edge`, or end of the earth, abbreviated to gaz (literally 28,819 ancient Greek miles [1](http://units.wikia.com/wiki/Mile_(Attic_Greek))[2](https://en.wikipedia.org/wiki/Earth), been one full revolution of the globe). Therefore a Gazillion has (28819 x 3) zeros and a Gazillion is...

1, {% include gazillion_zeros.html %}.0
