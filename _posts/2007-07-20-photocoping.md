---
title: Photocoping
author: Greg
layout: post
permalink: /2007/07/photocoping/
comments: True
categories:
  - Uncategorized
---
After feeling a little guilty for eating so much chocolate yesterday I decided to make my self feel better by tempting Trudy, who has more will power then me and has been on a diet after her holiday. I slowly pushed an open box of roses chocolates closer to her until she noticed them and lidded them and moved them far away. When she left her desk next, I scattered some chocolates on her desk but when she returned she didn&#8217;t crack, she just cleared them away and gave me a dirty look. The day continued and by 4 she&#8217;d broken and enjoyed the evil delicious chocolate. I smiled on the inside.

I&#8217;m feeling a little more comfortable at work and have started been a little mischievous. After using the printer I left it set to print 11 copies instead of the usual one. I went back to my desk so I didn&#8217;t get to enjoy the fruits of my labour but my next attempt led to Pat, the girl who sits next to me, returning to her desk with an annoyed look on her face and a hand full of paper. She exclaimed &#8220;I&#8217;ve got 10 of these stupid sheets cause someone didn&#8217;t return the photocopier to one sheet&#8221;. It made my day. Funnily enough I&#8217;ve been told that I can rub some people the wrong way&#8230;

I&#8217;m off to Germany tomorrow. Bremen, I&#8217;d never heard of it until I stumbled upon it on the easy jet web site. I&#8217;d never been to Germany so I thought I&#8217;d visit. I&#8217;m still not sure what I&#8217;ll do when I finish my work contract here. I&#8217;m tossing up between a Russia-China train trip and an Europe-India back packing trip. I&#8217;d love to do a Bollywood dance course in India. But the Trans-Siberian express would be a brilliant adventure too. hmmmm&#8230;. What do you think I should do?