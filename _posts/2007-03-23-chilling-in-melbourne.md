---
title: Chilling in Melbourne
author: Greg
layout: post
permalink: /2007/03/chilling-in-melbourne/
comments: True
categories:
  - Uncategorized
---
I just said goodbye to Ninny :'( That&#8217;s the last I&#8217;ll see of my family for a while. I&#8217;m off to Perth tomorrow then leaving for London on the 4th of April with my girlfriend Nathalie. I want to learn comedy in another language so I might be away for a while. It has been nice spending time with Ninny and Gerard before I go. Except Ninny took us out to a restaurant and failed to inform me that it was a vegetarian restaurant, so when I asked if the feta pasta salad had meat in it I was answered with a really slow “&#8230; This is a vegetarian restaurant”

I&#8217;m having coffee with an army mate now, Ninny and Gerard are at a wedding.