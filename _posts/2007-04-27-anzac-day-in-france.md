---
title: ANZAC day in France
author: Greg
layout: post
permalink: /2007/04/anzac-day-in-france/
comments: True
categories:
  - Uncategorized
---
I&#8217;m in a bar writing this blog entry analogically :( whilst reading the French comic Tin Tin (I&#8217;m up to the 5th box and the first two boxes didn&#8217;t have words in them) Nathalie is at work until 11 so I&#8217;m just hanging out to pick her up (by pick up I mean walk her home, we don&#8217;t have one of those fancy motor cars) She&#8217;s had a long day, she started at 9:30 with a two hour break. But she&#8217;s got other job offers so I don&#8217;t think she&#8217;s too fussed, it&#8217;s only a 3 day trial. I on the other hand am still unemployed but my French is improving and I think I should be job worthy within weeks :) I think :)

Last night at bar I am currently residing in, we meet a few people who bought us some local booze. The people in Cahors are really friendly once you explain you&#8217;re not English (I tell a white lie) A couple who spoke no English showed me a place where I can learn French, I&#8217;ve been doing a lot of paper talking (getting people to write down on paper what they are saying so I can look it up in my French/English dictionary) I long for English speaking company (apart from Nathalie) and the other night we found some in the form of an American staying at the hostel. Nat and I talked her ear off and we shared 2 bottles of Cahors cheapest (under 3e for a nice bottle).

We&#8217;re staying at a formule 1 hotel for 3 days. The hostel was booked out and we didn&#8217;t reserve enough in advance. It&#8217;s 29e for a room which can hold 3 people (although it&#8217;s only the two of us) verse 10e each for dorm accommodation at the hostel. We&#8217;ve applied for long stay accommodation at the hostel. It&#8217;s pretty good and comes with breakfast and dinner 5 days a week for 332e each a month.

My hand held GPS let us down yesterday. We were on our way to the formule 1 hotel from the hostel, a 1.5Km trip so my GPS friend told us but when we reached the supposed location of the hotel it was nowhere to be seen. I tried asking a local but unless they speak really slowly, using really basic words, and lots of hand gestures (and preferably speak in English) I struggle to understand. With Nathalie&#8217;s help (she speaks the tongue of the frog fluently) we discovered we were 4Km away from the hotel and were in fact walking in the wrong direction. But a gentleman by the name of Alex, after seeing how much baggage Nathalie was struggling to drag offered to drive us. Very helpful, we&#8217;ve got his number and will be buying him a drink.

I ate Brie cheese with a baguette for bread and cheese day (18th of April, the day we emigrated to Australia which we celebrate by eating bread and cheese as that was the food my father brought back for our first Aussie meal). And for ANZAC day I drank Fosters, real Australian :)

Well my hand hurts from analog typing so I&#8217;ll sing off here :)