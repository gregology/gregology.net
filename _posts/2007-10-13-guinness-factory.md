---
title: Guinness factory
author: Greg
layout: post
permalink: /2007/10/guinness-factory/
comments: True
categories:
  - Uncategorized
---
Chris, Scott, and I visited the Guinness factory with 3 of Chris&#8217; backpacker mates yesterday. We drank a lot of black and then hit up a crepe bar. The Guinness factory was pretty cool, it cost 14e and we got at least 3 pints so it was a really good deal, plus now I know how they make barrels.

Today was Jacquis birthday so we made chicken burgers and lamingtons. Jacqui cleaned the kitchen because we&#8217;re boys and don&#8217;t do that kind of stuff.

We&#8217;re heading off tomorrow around Ireland if we can get a car so I will be out of blog range for about 2 weeks.