---
title: No smoking
author: Greg
layout: post
permalink: /2007/07/no-smoking/
comments: True
categories:
  - Travel
  - Work
---
They&#8217;ve just brought in a smoking ban in the UK which means that all the pubs will smell of the soap dodger BO instead of smoke. It&#8217;s going to affect business so a guy with a slide show told me in a meeting at work (I now work in the industry, I&#8217;m in accounts payable of a pub company). I really enjoyed the slide show, it meant that I didn&#8217;t have to work for 30 mins but even better than that my name, among others, was on the slide show as a new member to the team. The gentleman at the front of the meeting asked everyone to give the new members a round of applause. I wasn&#8217;t sure if I was meant to applaud the other members or just stand there and make the most of been applauded. So I stood there uncomfortably smiling and did a half arsed leg tap. Angie wasn&#8217;t invited to the meeting because she&#8217;s finishing up tomorrow. I&#8217;ve been avoiding eye contact and have found a longer but less dangerous way to the coffee machine. I&#8217;m still quite scared that she&#8217;s going to stab me tomorrow. It&#8217;d better wear an under shirt.

I went Ceroc dancing with Haley and Laura tonight. It was fun and interesting to see the different way they dance here. They have a different dance culture which I wasn&#8217;t aware of and which made for an embarrassing situation when I told a lady, who I later found out was an instructor, that she didn&#8217;t know what she was talking about and that I&#8217;ve been dancing for a year now.

Nathalie has gone back to Australia because she was quite ill :( It&#8217;s good that she&#8217;s back home with her family and I&#8217;m sure she&#8217;ll be well again shortly so I&#8217;m not too worried. Her job in Greece turned out to be a bit of a dud. The place was nice but soulless, and her health insurance didn&#8217;t start until September, which is fine if you&#8217;re not sick but if that was true you wouldn&#8217;t need health insurance. Bob Fillis, the gentleman who employed her wasn&#8217;t happy but he&#8217;s Greek so it doesn&#8217;t matter.