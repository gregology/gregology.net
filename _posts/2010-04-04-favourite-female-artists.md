---
title: Favourite female artists
author: Greg
layout: post
permalink: /2010/04/favourite-female-artists/
wordbooker_options:
  - 'a:10:{s:18:"wordbook_noncename";s:10:"bb2781d14b";s:18:"wordbook_page_post";s:4:"-100";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_default_author";s:1:"2";s:23:"wordbook_extract_length";s:3:"256";s:19:"wordbook_actionlink";s:3:"300";s:26:"wordbooker_publish_default";s:2:"on";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:20:"wordbook_comment_get";s:2:"on";}'
comments: True
categories:
  - links
  - Music
tags:
  - Alizee
  - Camille
  - Carla Bruni
  - Emiliana Torrini
  - females artists
  - Katie Melua
  - Ladyhawke
  - Lily Allen
  - Lisa Mitchell
  - Music
  - music videos
  - 'Peter Bjorn &amp; John'
  - playlist
  - Regina Spektor
  - Sia
  - Soko
  - videos
  - YouTube
---
Here are my favourite female artists I&#8217;ve found on YouTube.  
The playlist:  
1. Sia &#8211; You&#8217;ve Changed &#8211; USA (This is an awesome film clip so a good starting point)  
2. Lisa Mitchell &#8211; Coin Laundry &#8211; Australia (I fell in love with Lisa after seeing this film clip, wow)  
3. Carla Bruni &#8211; Quelqu&#8217;un m&#8217;a dit &#8211; France (She has some amazing songs and some amazing film clips)  
4. Regina Spektor &#8211; On The Radio &#8211; USA (born in Russian SSR)  
5. Soko &#8211; Dandy Cowboys &#8211; France (Soko really sums up crazy chicks, I love her. I really like the YouTube film clip too)  
6. Alizée &#8211; J&#8217;en Ai Marre &#8211; France(wow, she&#8217;s very sexy but I&#8217;m still cultured by watching her because she&#8217;s French)  
7. Peter, Bjorn & John &#8211; Young Folks &#8211; Sweden (I am aware the there is a male singer here too :) )  
8. Camille &#8211; Ta Douleur &#8211; French (She mixes some really weird sounds)  
9. Ladyhawke &#8211; Paris Is Burning &#8211; New Zealand (Pretty cool song)  
10. Emilíana Torrini &#8211; Jungle Drum &#8211; Iceland (Good song, cool film clip)  
11. Lily Allen &#8211; LDN &#8211; England (I find her really sexy for some reason, which I don&#8217;t understand because she&#8217;s not that attractive, she doesn&#8217;t hold her self well at all, and has an awful accent)  
12. Katie Melua &#8211; Shy Boy &#8211; England (Born in Georgian SSR)

These are not in order of preference, just an order that I think will make them most enjoyable. These song are not necessarily my favourite song by each artist, again the song was chosen to fit into a playlist. Also, I&#8217;ve kept a few artists out of this list such as MIA, Blondie, Shakira, Lady Gaga, and Katy Perry because I didn&#8217;t want anyone knowing I was a fan :) If you know of any similar artists that I might like, please tell me in the comments.

Enjoy!
