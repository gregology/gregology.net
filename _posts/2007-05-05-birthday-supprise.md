---
title: Birthday supprise
author: Greg
layout: post
permalink: /2007/05/birthday-supprise/
comments: True
categories:
  - Uncategorized
---
I am writing this blog entry from the discomfort of a toilet paying for my birthdays wonderful cheesefest (isn&#8217;t technology grand) I ate about a kilo of cheese on my birthday and the following day and it does bad things to your insides. Maybe I shouldn&#8217;t have eaten the wax as well :s Nathalie had to work on my birthday so I spent the day inspecting the towns wall while enjoying the hardest cheese I could find (I&#8217;m a little over soft cheese, I just wanted cheader or parmigiana) it wasn;t very hard. Nathalie came home with lots of little cakes, bread & cheese (which although made me burst a blood vessel was very tasty and well worth it), and a wizard figure for my nefew or niece (i want to build him or her a castle or palace, but either way a wizard fits in with both). We lit the candles (20 candles not 24, it made me feel better) and I made my wish. Then it was pizza, beer, and dancing :)

Nathalie&#8217;s 22nd birthday was the day before mine on the 2nd of may. It was also the day she started her new job at au buerau (I think that&#8217;s how you spell it, it means the office) which she seems to enjoy. Because she was at work I had a lot more time to prepare. I wanted to buy her party hats and but they don&#8217;t sell them in super markets here so I decided to make some. I bought some cardboard and sticky tape and with the help of a friendly Germans toe nail clippers I had two fancy looking hats. I also got her sims 2, an odd game where you control peoples lives, it&#8217;s like a virtual doll house for grown ups. We ate ice cream cake, drank cahors wine, and then pizza.

We&#8217;ve both been sick for the last few days which isn&#8217;t fun when it&#8217;s your birthdays plus in Nat&#8217;s case, you&#8217;re starting a new job. I was sick first so I have to live with the guilt that I made Nat sick :(

Well I&#8217;m going to shower and go visit Nat at her work.