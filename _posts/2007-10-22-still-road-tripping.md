---
title: Still road tripping
author: Greg
layout: post
permalink: /2007/10/still-road-tripping/
comments: True
categories:
  - Uncategorized
---
we&#8217;re in doolin at the moment about to head off to see some caves in the rain. We visited Trim, where my grandmother was born and found her mothers grave. I didn&#8217;t know where the grave was and we played a morbid game of wheres wally. Chris won. I took some photos and the grave was well tended which was nice to know.

Saturday night was spent in Galway which is a party town but I wasn&#8217;t feeling so good so I skived.