---
title: Not dead, just lazy
author: Greg
layout: post
permalink: /2007/08/not-dead-just-lazy/
comments: True
categories:
  - General
  - Travel
---
I&#8217;m not dead, just lazy. Hayley and I visited Amsterdam 2 weekends ago. Very interesting 😉 we stayed at a Christian youth hostel which was conveniently located next to a Thai massage parlor and next door to that were pretty red lights denoting a lady who would make you feel happy for 50 eruos&#8230; So i over heard. The district was packed from 9pm till late and Hayley and I spent every night strolling past the ladies. i especially liked the weird exotic fetish&#8217;s, like the big mumma&#8217;s and my favorite, the frog eyed lady friend.

Hayley and I were forced to take advantage of the day due to the Christians 2am curfew, so on Saturday we went on a free walking tour offered by the hostel. As expected it was old bridge, Nazi, and jesus heavy which left out the red light district. Still, it was quite interesting and Christianity was a large part of the cities history so i took some photo&#8217;s of JC and I. We spent the rest of the day exploring Amsterdam&#8217;s canals on a leg powered paddle boat.

Day two was spent getting a sore bum on a tandem bike. As with the paddle boat I did more then my fair share of peddling :s the bike took us to the art gallery which was full of paintings of battles won and epic conquests for mysterious new lands. It was incredibly busy and i couldn&#8217;t have a painting to myself :(

On our last night we strolled the district so Hayley could have one last stare. Thanks to goofy looking ponchos and a heavy down pour we had the red lights to our self. The ponchos acted like blinkers on horse making Hayley&#8217;s unsubtle staring even more obvious, and because of our matching film 4 poncho&#8217;s I couldn&#8217;t even pull the &#8220;she&#8217;s not with me&#8221; look.

Over the past few weekends I&#8217;ve been playing chief for the Grinyer&#8217;s. Aussie burgers came first and went down well, sushi next without as much success, then last night, summer spring rolls with satay dipping sauce and optional chili (I ate a lot of chili which hurt a lot more coming out then it did going in). For the sushi and spring rolls ingredients i had to wonder the peculiar isles of the Chinese supermarket. The same ingredients in Australia can be found in any supermarket because Australia has a lot heavier oriental influence so I&#8217;m excited to see what Indian influenced foods england has to offer that I haven&#8217;t discovered at home. In the Chinese supermarket I bought what I needed and also a surprise can of drink that had bits of jelly in it. It tasted&#8230; interesting but i wanted to appear open minded so I drank half of it.