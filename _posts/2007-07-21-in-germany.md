---
title: In Germany
author: Greg
layout: post
permalink: /2007/07/in-germany/
comments: True
categories:
  - Uncategorized
---
I&#8217;m in Germany :) So far so good. I&#8217;m blogging from my phone because my GPS PDA is loosing battery and it&#8217;s my way to the hostel. I sat next to a German who works for Macquarie Bank and an English woman who sleeps with her mouth open. I spoke my first German words in Germany when I came through customs, I said &#8216;bitter&#8217;. I was very chuffed with myself. The only other German words I know are from Rammstein songs so I&#8217;m not sure what kind of friends I&#8217;ll make here. I&#8217;ve just gotten on a fancy tram. I haven&#8217;t got a ticket but I&#8217;ll play the &#8216;I&#8217;m Australian&#8217; card and give them a smile, and if that fails I&#8217;ll pull some Rammstein lyrics and they&#8217;ll think I&#8217;m manically depressed and leave me alone.