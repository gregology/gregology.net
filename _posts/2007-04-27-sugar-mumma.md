---
title: Sugar mumma
author: Greg
layout: post
permalink: /2007/04/sugar-mumma/
comments: True
categories:
  - Uncategorized
---
I just dropped Nathalie at work for her second day. She doesn&#8217;t really like it there, it&#8217;s a really snooty French restaurant but she&#8217;s got another job offer starting on Wednesday and this is good experience for her. Plus it&#8217;s money and how can she be my sugar mumma if she&#8217;s not earning? I watching French TV and I&#8217;m a bit over not knowing what&#8217;s going on :(

I was sitting at a fountain reading my Tin Tin comic when 5 homeless looking ultra hippies, maybe Gipsy&#8217;s came with there pack of about 10 dogs. They pushed the dogs in the fountain, I assume to wash them, and then pulled them out. The dogs seemed in better condition then the people but were clearly scared of them and cowered when a fast movement was made. These people keep dogs with them so they cannot be arrested as the police need a vet present to arrest someone with a pet so you see a lot of these people with dogs, rabbits, cats, etc so I&#8217;ve been told. It was an odd sight so I took a photo.

France has a lack of public toilets and it gives the streets a distinct smell of urine. On my way back to our hotel today after dropping Nathalie off I saw an old man taking a slash in a car park. I was a little confused and I think I was staring. I did find what I think/hope was a public toilet. It was very odd and the troff was clearly visible from outside. I took my chances anyway.

We&#8217;re currently staying about 2.5Km out of town and it&#8217;s very country. There is a field next door with 3 horses in it, 2 white and a black. I don&#8217;t like the black one. We fed them carrots and took some photos of them. They don&#8217;t have horse shoes and there hooves are all cracked. Maybe that&#8217;s why horse shoes are good luck, because they stop your hooves from cracking. Maybe they are food horses and that&#8217;s why they don&#8217;t have shoes. I hope the black one is food but I&#8217;ll miss the 2 white ones. I named them.