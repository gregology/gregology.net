---
title: Experimenting with squatting
author: Greg
layout: post
permalink: /2010/06/experimenting-with-squatting/
comments: True
categories:
  - Health
tags:
  - health
  - poo
  - signs
  - squatting
  - Uni Work
---
The toilet stalls at uni have anti squatting signs similar to this one.  
<img src="/wp-content/uploads/2010/06/NoSquatting.jpg" alt="" title="NoSquatting" width="100%" height="auto" />

At first I thought the idea of squatting on a toilet was silly but I don&#8217;t like blindly obeying signs and I like to try new things so&#8230;

<img src="/wp-content/uploads/2010/06/P6210072.resized.jpg" alt="" title="Squatting on a toilet" width="100%" height="auto" />

And I&#8217;m glad I did, it is amazing! I have never felt so empty. After a little research I found out that squatting is the [healthier option][1] too. It minimises &#8220;fecal stagnation&#8221; (which just sounds awful) and reduces the risk of colon cancer, colitis, Crohn&#8217;s disease, contamination of the small intestine, diverticulosis, gynecological disorders, heart attacks, hemorrhoids, hiatus hernia and GERD, pregnancy and childbirth issues, prostate disorders, and sexual dysfunction!

Tips for new squatters; aim forward and make sure there is a toilet brush available.

Yet again, not obeying a sign has worked out well for me.

 [1]: http://www.naturesplatform.com/health_benefits.html
