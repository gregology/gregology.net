---
title: Summers in England
author: Greg
layout: post
permalink: /2007/08/summers-in-england/
comments: True
categories:
  - Travel
  - Work
---
The sun is on my back, water has changed to its liquid state and I&#8217;m enjoying the finest summer day england has offered me yet. It&#8217;s been a long slow week because i haven&#8217;t been feeling well, I&#8217;m a little run down after 4 weekends back to back so i declined Jamie&#8217;s offer of a drinking session tonight in favor of bed rest&#8230; And maybe grease the musical on dvd.

On Tuesday Trudy tasked me with the challenging job of looking for the word coffee in 800 invoices. It was something different so i eagerly searched the pile. After about 200 invoices and not one containing the word coffee, Trudy appears with a smile and says &#8220;don&#8217;t worry about that, I&#8217;ve got something else for you to do&#8221;. Later she informed me that the invoices containing the word coffee were at the bottom, a fact that she claims she didn&#8217;t know before tasking me, but i know better. Phase one of my revenge came today when she was training the latest Angie replacement, Patricia. The email system at work pops up any received emails with with the subject and first few words of the email. So while Patricia was in front of Trudy&#8217;s computer being trained I sent Trudy an email regarding her human flea treatment. Phase two won&#8217;t be noticeable until mid august.

I went Ceroc dancing on Tuesday and applied to be a dance buddy. If i&#8217;m successful I&#8217;ll help the new comers on Tuesday night but more importantly i can say with an arrogant dancers posture &#8220;i teach dance&#8221;.

Wednesday and Thursday went by slowly because my eyes found a clock on the wall. I&#8217;d disabled my computer clock and turn my phones display away unless the time is favorable to me. I helped speed up time by drinking lots of hot chocolate, which led me to a side by side comparison of chocomilk vs hot chocolate, two drinks competing in the vending machine for my tummy. Hot chocolate won with its superior chocolate flavour and lack of sickly after taste, followed by chocomilk, followed finally by the 50/50 mix which had the worst of both beverages.