---
title: Train Station Hotel
author: Greg
layout: post
permalink: /2007/11/train-station-hotel/
comments: True
categories:
  - Uncategorized
---
It&#8217;s raining and the nearest hostel is 3km away so unless it clears up soon tonight my home is a train station.