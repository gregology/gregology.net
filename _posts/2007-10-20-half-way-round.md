---
title: Half way round
author: Greg
layout: post
permalink: /2007/10/half-way-round/
comments: True
categories:
  - Uncategorized
---
We&#8217;re back in Dublin and we&#8217;re continuing our trip around Ireland. We went north spending 2 nights in Belfast, 1 night in Portstewart, and 2 nights in Westport. My photos will tell more then my words. I will write more when I get a chance.

Check out Scotts website, www.scottmc.com.au (deprecated) for photos on the fly.
