---
title: Nothing much
author: Greg
layout: post
permalink: /2007/05/nothing-much/
comments: True
categories:
  - Uncategorized
---
I haven&#8217;t blogged for a while because I haven&#8217;t done a hell of a lot. Nathalie has been working and I&#8217;ve been a house husband :) We&#8217;ve got a residency at the hostel. It&#8217;s really cool, we get our own room and they feed us breakfast every day and dinner every weekday, but best of all, our room has a bidet! Nathalie said I can&#8217;t wizz in it :(  
We&#8217;re off to Greece on the 12th for Nathalie&#8217;s English teaching job. It&#8217;s very exciting, i&#8217;ll have to start again learning a whole new language. I&#8217;ve been in France for under 2 months and can get by on my own so I&#8217;m sure I&#8217;ll be alright. The place we&#8217;re going is on the water so I&#8217;m going to enjoy some much needed swimming, lots of cheese makes me happy but it&#8217;s starting to show.