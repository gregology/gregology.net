---
title: Ninemsn Spin
author: Greg
layout: post
permalink: /2010/05/ninemsn-spin/
comments: True
categories:
  - politics
tags:
  - ninemsn
  - politics
  - spin
  - tony abbott
---
I found an interesting spin video on ninemsn (video removed). The title &#8220;Abbott admits to policy lies&#8221; interested me enough to click. After watching the [7:30 Report][2] I figured out what actually happened.  
[<img src="/wp-content/uploads/2010/05/Screenshot.png" alt="" title="Tony Abbott 7:30 Report" width="100%" height="auto" />][3]  
(Notice the tie&#8230;)  
Tony said;  
*&#8220;The statements that need to be taken absolutely as gospel truth are those carefully prepared, scripted remarks&#8221;*.  
Ninemsn reported this in a very colourful way. Their report started with the;  
*&#8220;Shadow treasurer Joe Hockey has defended opposition leader Tony Abbott&#8217;s&#8230;&#8221;*  
A clip from QandA was shown where Joe attacks Kevin for breaking his promises. The tale ended with images of Tony at a charity dinner and;  
*&#8220;&#8230;the event is aimed at raising money for youth cancer.&#8221;*  
To be fair, they did make mention of Kevin&#8217;s attendance at the dinner too but this was done while showing images of Tony talking to an attractive lady and no actually footage of Kevin at the event was shown. Ninemsn managed to report a story about a politician admitting they sometimes lie without actually showing the gaff. Well done! This is the motivation I need for my vote for policies site.

 [2]: http://www.abc.net.au/news/video/2010/05/17/2901971.htm
 [3]: /wp-content/uploads/2010/05/Screenshot.png
