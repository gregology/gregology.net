---
title: Chiling at the airport
author: Greg
layout: post
permalink: /2007/04/chiling-at-the-airport/
comments: True
categories:
  - Uncategorized
---
Although my web site would have you believe I&#8217;m currently in flight from Kuala Lumpar to London I&#8217;m really just at the airport and the mistake is due to stupid time zones and my lack of being able to figure them out :s

We&#8217;ve got an hour and a half here and they have free internet. The airport at least is quite western but a lot of the women have head scarfs. I&#8217;m wearing a shirt that says &#8220;Jesus Loves Greg Most&#8221; and I got some weird looks from the flight attendent as I left. Maybe he doesn&#8217;t understand my British humour.

I&#8217;ve got a money belt that I was quite impressed with and used for my passports when going through customs at Perth International. The customs officer told me to be carefully with it as it may be misconstrude as a bomb or something. I laughed but she was quite serious. Billions of dollars, thousands dead, on the war on terror and I don&#8217;t even have the freedom to wear a goofy money belt :( Where is the justice?