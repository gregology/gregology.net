---
title: From Renee Bracks friend
author: Greg
layout: post
permalink: /2009/02/from-renee-bracks-friend/
comments: True
categories:
  - Uncategorized
---
&#8220;Compassion is caring for someone you don&#8217;t know&#8221; &#8211; Renee Bracks friend (I forget who exactly, I was a little drunk when I heard it :))

It&#8217;s easy to care for people you love but the ability to care for people you don&#8217;t know is something special.