---
title: 'Greg&#8217;s famous'
author: Greg
layout: post
permalink: /2009/02/gregs-famous/
comments: True
categories:
  - Army
  - Travel
---
This week I appeared in the Army Newspaper in the Our People section. So I&#8217;m pretty much famous now. I signed all the papers I could find with phrases like &#8220;reach of the stars&#8221;. Being in the paper means I owe beers and everyone is trying to call me up on it.

My mum sent me more presents today :) My family have been really good to me, so supportive, they&#8217;ve sent me a couple dozen packages. It is a real morale boost. Sam and Aaron sent me a Christmas Tree and a framed picture of Zander which is in my room, Ninny and Gerard sent me some funky clothes, Stu and Janell sent me a book on trading on the stock market, and my mum has sent me heaps and heaps of stuff. An old school coffee grinder, a milk frother, heaps and heaps of food (I think she&#8217;s worried that they&#8217;re not feeding me here). Nathalie sent me a wine set and some art supplies, and a girl from my unit sent some home made cookies. I think I&#8217;ve received the most packages :)

Sam is due shortly and Ninny is due later this year. I&#8217;m very excited about my new nephews or nieces. Zander is trying to walk and Oliver is up and about. I&#8217;m sad that I&#8217;m missing all that :( and I just found out that today that my application to the Australian Antarctic program has progressed to the next selection stage which may mean another 12 months away. I&#8217;m very lucky to have such a supportive family.

Trivia was great. The team name I suggested was altered from &#8220;Clarkey and Friends&#8221; to &#8220;Clarkey has no friends&#8221; by a democratic decision :( but I received a $50 pity drink voucher from the MC. We came third last but at least there are people dumber than us and I&#8217;m a lesser man so that means a lot to me. Last night we had a group of performers from Australia come and entertain us. The line up was singer/song writer Mark Wilkinson, the band Asleep in the Park, Comedian Dave Jory, and MCing the night was television presenter Renee Brack (she was the lady who interview Chopper Reid). We were allowed to drink so we all knocked back and few and had a really good night. The MC said a quote that really moved me. &#8220;Real compassion is caring from someone you don&#8217;t know&#8221; she was making a reference to our efforts here in the Solomon Islands. After the last set was played I showed off some moves with some of the people I&#8217;d been learning latin dance with. We partied until midnight then we&#8217;re sent to bed but we got up to more shenagians in the lines.

Danni got &#8220;food poising&#8221; last night and is in the medical centre so I&#8217;m covering her shift. It&#8217;s not too bad because I hadn&#8217;t successfully gotten out of the graveyard sleep pattern yet :) But I really haven&#8217;t had much sleep lately. I found out that I was working tonight this morning and I had to go on a helicopter flight so I only got my head down for a couple of hours. I was feeling really seedy in the chopper :( I tried to sleep but the window next to me said &#8220;emergency exit, push&#8221; so I didn&#8217;t want to put my head against it.

I think I&#8217;ve done pretty well blogging at this wee hour.