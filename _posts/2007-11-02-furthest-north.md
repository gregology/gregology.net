---
title: Furthest north
author: Greg
layout: post
permalink: /2007/11/furthest-north/
comments: True
categories:
  - Uncategorized
---
I&#8217;ve decided to head back to Göteborg tonight as there were no trains to Stockholm. I had a wonder around the city but was limited by my pack. The train ride here was awesome, lots of beautiful lakes but now the sun is sleeping so I&#8217;ll get the stars on my return. This is the furthest north I&#8217;ve been and I&#8217;m feeling it. My hands are numb.