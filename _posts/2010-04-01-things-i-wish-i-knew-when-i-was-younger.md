---
title: Things I wish I knew when I was younger
author: Greg
layout: post
permalink: /2010/04/things-i-wish-i-knew-when-i-was-younger/
wordbooker_options:
  - 'a:11:{s:18:"wordbook_noncename";s:10:"d92ad6ee12";s:18:"wordbook_page_post";s:4:"-100";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_default_author";s:1:"2";s:23:"wordbook_extract_length";s:3:"256";s:19:"wordbook_actionlink";s:3:"300";s:26:"wordbooker_publish_default";s:2:"on";s:20:"wordbook_comment_get";s:2:"on";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:24:"wordbooker_status_update";s:2:"on";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";}'
comments: True
categories:
  - Philosophy
tags:
  - Philosophy
---
Legal doesn&#8217;t mean moral  
Illegal doesn&#8217;t mean immoral  
You can&#8217;t experience non existence so don&#8217;t worry about death  
Your purpose in life is to improve the universe  
You are always completely free even if your choices are limited  
The easiest way to improve the universe is to smile  
Dancing is fun  
School isn&#8217;t the best years of your life  
Just because someone tells you something confidently, doesn&#8217;t make it true  
If you&#8217;re telling a lie, say it confidently  
Trying lots of different things shows more character then sticking to something you don&#8217;t enjoy  
Don&#8217;t bother chasing girls, by the time your ready to settle down they&#8217;ll be chasing you  
Learn to think rationally  
The bible doesn&#8217;t make any sense  
Be confident, what&#8217;s the worst that can happen  
There is no shame seeking help  
The majority aren&#8217;t always right  
You don&#8217;t need a reason to laugh  
If someone hurts you, tell them  
A group of people are dumb  
There are many norms in our culture which are really stupid  
Don&#8217;t judge people but don&#8217;t let people push you around  
Write your thoughts down  
You don&#8217;t have to plan your life in your teens but a little direction is always good  
Try to understand how others may perceive your actions  
People only seem irrational because you have not had their life experiences  
Your penis has very short term goals that probably aren&#8217;t conducive to achieving your long term goals