---
title: Moved again
author: Greg
layout: post
permalink: /2007/07/moved-again/
comments: True
categories:
  - Uncategorized
---
I don&#8217;t think Anne got much work done last week and people must have noticed because I was moved again today. I&#8217;m sitting on the other side of the office in Angie&#8217;s old desk. When I turned on the computer her name appeared in the login window. I felt a chill down my spin and quickly deleted it. I&#8217;m sitting next to a new fellow, Scott. After my last experience it took he a while to build up the courage to ask him if he wants to file together but to my surprise he replied softly &#8220;OK&#8221;. 

I start open uni soon. My first course is called Globalisation: The Asia Pacific and Australia through Griffith University. I&#8217;ve never studied by correspondence and I was concerned that I wouldn&#8217;t have the initiative or will power to do well, but then I realised that at Newcastle University I didn&#8217;t have the initiative or will power to do well :)