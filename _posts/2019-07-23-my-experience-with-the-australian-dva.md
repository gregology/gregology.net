---
title: "My Experience with the Australian DVA"
author: Greg
layout: post
permalink: /2019/07/my-experience-with-the-australian-dva/
date: 2019-07-23 19:00:00 -0400
comments: True
licence: Creative Commons
categories:
  - Army
tags:
  - DVA
  - Department of Veterans Affairs
  - Australian Army
  - Army
---

<script type = "text/javascript" >
  var days = ((new Date() - new Date(2019,2,19)) / (1000 * 60 * 60 * 24)).toFixed(0);
  
  //To display the final_result value 
  document.write("<b>tl;dr</b> The Australian DVA is bureaucratic, incompetent, and failing veterans. It's been " + days + " days since I reached out for help with insomnia as a result of my time in Afghanistan. So far I have not received any help or treatment from the DVA."); 
</script>

In 2010 I deployed to Afghanistan with the Australian Army. We worked in shifts - 8 hours on, 8 hours off - for months. Humans were not designed for 16 hour sleep cycles. Since returning home in 2011 I have suffered insomnia. Canada has been my home since 2012 when I immigrated here for work. Luckily The Australian Department of Veteran Affairs (DVA) and Veterans Affairs Canada have [reciprocal relationships](https://www.veterans.gc.ca/eng/about-vac/legislation-policies/policies/document/1103) for veteran services. On March 19th, 2019, I decided to seek help from the Australian DVA. My encounters with the Australian DVA in the past have been awful so I decided to document my (not uncommon) experience. This is my unsuccessful journey for help from the Australia DVA so far.

#### 19th of March, 2019 (Day 0)

After years of dealing with insomnia, I decided to reach out for help. I called Veterans Affairs Canada and was informed that I would have to initiate the process though DVA Australia.

#### 20th of March, 2019 (Day 1)

Called DVA Australia - Mary told me the reciprocal relationship was only one way and that I would not be eligible for treatment. Here is the complete conversation including 4 minutes of been told that Australia and Canada have a one way reciprocal relationship.

<iframe width="560" height="315" src="https://www.youtube.com/embed/I9b4f_kR4wc?start=490" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

#### 21st of March, 2019 (Day 2)

Obviously I was not happy with the initial response from the DVA. I registered with [My Service](https://www.dva.gov.au/myservice/) and made my insomnia claim online. 

I called the DVA again hoping that I would speak to someone else that would be willing to help me. Amelia from DVA Australia connected me with the section of DVA Australia that deals with overseas veterans.

I completed a load of paperwork and was put in contact with Karen.

I also made a complaint with the DVA about my call with Mary:

>Hey Team,
>
>I am an Australian Veteran living in Canada. I was after information regarding Australian & Canada's reciprocal DVA relationship. I had reached out to DVA Canada and was told to initiate the process with DVA Australia.
>
>This morning I called DVA Australia and had a difficult conversation with Mary. Mary explained to me that the reciprocal relationship would only work if I were a Canadian veteran living in Australia, not the other way around. When I explained that that's not reciprocal, she reiterated that it doesn't work in reverse.
>
>I ended up leaving the conversation and calling back. The second time I got through to someone who was willing and able to assist me.
>
>I don't expect DVA staff to know everything but it's unacceptable for them to lie when they don't know something. It was 17 minutes of condescendingly being incorrectly explained how the reciprocal relationship works. A simple, "I don't know, I'll find someone who does" would have solved the issue.
>
>I find it difficult to articulate how disappointed I have been with DVA Australia. I've needed help twice as a veteran and both times I've had to deal with unprofessional people like this on the phone system. As a result of my horrible first experience, I started recording my interactions with the DVA. I'm happy to share my recording of my phone call with Mary.
>
>I have service related problems that I want to get addressed. They're not life threatening but they do affect my quality of life. It pains me to think of my mates with more sever issues using DVA services and dealing with this unprofessional behaviour.
>
>What steps are you going to take to rectify this situation?
>
>Cheers,
>
>Greg.

I received a response the same day

>Hi Greg
>
>I’ve had a quick look into this (through your DVA file number, TSM06858) and unfortunately there’s no record in DVA logging systems of the ‘Mary’ you spoke with
>
>Do you recall what area or section of DVA Mary was from?  It looks like you may have been assisted the second time by Bernadette of the Melbourne office?
>
>I would like to pursue this matter on your behalf with Mary’s supervisor, but I need some more information I’m afraid as there are a lot of different Mary’s on staff (as you can imagine) …
>
>regards
>David
>
>David ******  
>Feedback Management Officer  
>Department of Veterans’ Affairs

and responded

>Cheers David,
>
>Yes, I believe the second person I spoke to was Bernadette. Bernadette was very helpful and knowledge. I hadn't completed my dva application when I was speaking to Mary so I imagine that's why I'm not showing up in the system. Mary was in the Melbourne office and our call began around 08:15EST. Does that help?
>
>Thanks again for looking into this, it helps put my mind at ease knowing that these matters are taken seriously.

As of October 2019, I still haven't received a response from David

#### 22nd of March, 2019 (Day 3)

Received confirmation that my application had been received and was being processed.

>Dear Greg,
>
>Thank you for your recent request to the Department of Veterans’ Affairs (DVA) for prior approval of treatment or services. Please log into your My Service Account as your letter has been sent to you.
>
>I will update you once I have more information. They have advised that you check you my service account regularly as they will be communicating with you there regarding your claims.
>
>Thankyou &  
>Kind regards,  
>Karen  
>Department of Veterans' Affairs  
>Health Approvals & Home Care

#### 11th of April, 2019 (Day 23)

Received an email saying my claim was being investigated. I was feeling positive that DVA Australia aims to finialise 50% of their cases within 100 days of lodgement.

>Good Morning Gregory,
>
>This is a courtesy email to advise you that I will be assisting in the investigation of your MRCA Initial Liability claim for the condition of: Insomnia & Mental Health
>
>DVA aims to finalise approximately 50% of cases within 100 days of lodgement, but it could also take longer, depending on the complexity of your claim.
>
>(Please refer to link: https://www.dva.gov.au/about-dva/overview/dva-service-charter for more information regarding DVA’s Service Charter)
>
>Would you please be able to respond to this email to advise if you have seen or are currently seeing a Psychiatrist in regards to this claimed condition?
>
>If you have any questions in regards to your claim, please do not hesitate to contact me on the details listed below.
>
>With Kind Regards,
>
>Vijayata  
>Delegate, Liability  
>Primary Claims | Clients’ Benefits | Sydney Office  
>Department of Veterans’ Affairs

There were a couple of emails back and forth that day, and we settled on having an assessment from a local psychiatrist.

#### 16th of May, 2019 (Day 58)

Received an update a month later that the DVA was still trying to setup the appointment

>Good Morning Greg,
>
>I’m just touching base to let you know that MLCOA is still in the process of liaising with their Canadian counterpart and arranging an appointment for you.
>
>I’ll keep you updated.
>
>With Kind Regards,
>
>Vijayata  
>Delegate, Liability  
>Primary Claims | Clients’ Benefits | Sydney Office  
>Department of Veterans’ Affairs

#### 20th of May, 2019 (Day 62)

Received this email about my claim.

>Good morning Mr Clarke,
>
>My name is Hugh ******, from the Department of Veterans’ Affairs.
>
>I’m contacting you today as part of the MyService team to inform you that your claim, submitted 10/04/2019, had been stalled as a result of a technical issue on our end. This issue has since been identified and resolved, so your claim has moved on to being processed as a priority.
>
>We apologise for any inconvenience in the delay.
>
>Regards,
>
>Hugh ****** | Administration Officer  
>Digital Client Experience  
>Department of Veterans’ Affairs | www.dva.gov.au

#### 31st of May, 2019 (Day 73)

Received this follow up email about an appointment.

>Good Afternoon Greg,
>
>I have heard back from MLCOA. They have provided an appointment for you on Wednesday 5th July 2019 at 10am.
>
>This appointment will be with Dr Ken Suddaby MD, FRCPC
>
>Location: Ottawa, Canada
>
>Speciality: Psychiatrist
>
>Please advise if this appointment is suitable for you.
>
>With Kind Regards,
>
>Vijayata  
>Delegate, Liability  
>Processing Team 2 | Sydney Office | Clients’ Benefits  
>Department of Veterans’ Affairs

#### 1st of June, 2019 (Day 74)

I confirmed and clarified the date (as the 5th July 2019 isn't a Wednesday).

>Thank you Vijayata,
>
>I assume that's Wednesday the 5th of June? Either way, the appointment is suitable for me on the 5th of June or the 5th of July.
>
>Thanks again for lining this up,
>
>Greg.

#### 2nd of June, 2019 (Day 75)

Received another email with the incorrect date.

>Good Morning Greg,
>
>The email I received from MLCOA read:
>
>“Our sister company have advised that Dr Suddaby is happy to accept this referral and has offered an appointment on Wednesday 5th July 2019 at 10am.
>
>Can you please liaise with Mr Clarke to see if he is available to attend this appointment?”
>
>The appointment would be 5th July 2019 at 10 am.
>
>I will let MLCOA know so they can book this appointment for you.
>
>With Kind Regards,
>
>Vijayata  
>Delegate, Liability  
>Processing Team 2 | Sydney Office | Clients’ Benefits  
>Department of Veterans’ Affairs

#### 3rd of June, 2019 (Day 76)

Again, I tried to confirm the correct date.

>Hello Vijayata,
>
>Wednesday the 5th of July 2019 is not a correct date. The 5th of July is a Friday, however the 5th of June is a Wednesday. Either the day or the month is wrong. Can you please confirm the actual date of the appointment.
>
>Cheers,
>
>Greg.

#### 3rd of June, 2019 (Day 76)

Correct date was confirmed.

>Good Morning Greg,
>
>Please see email below, your appointment has been scheduled for Friday 5th July 2019 at 10 am.
>
>My apologises for the confusion.
>
>With Kind Regards,
>
>Vijayata  
>Delegate, Liability  
>Processing Team 2 | Sydney Office | Clients’ Benefits  
>Department of Veterans’ Affairs

#### 11th of June, 2019 (Day 84)

Received another email requesting that I confirm the same appointment again.

>Good morning Greg
>
>A tentative appointment has been scheduled for you to meet with Dr ******, on Friday 5th July 2019 at 10am to assist with the investigation of your outstanding claim with DVA.
>
>Could you please advise as soon as possible as to whether you are available for this appointment.
>
>For your information:  
>Dr Ken ******,  
>MD, FRCPC  
>Location: Ottawa, Canada  
>Speciality: Psychiatrist  
>
>Kind regards,
>
>Alicen  
>Senior Delegate - Liability  
>Processing Team 2 | Sydney Office | Clients’ Benefits  
>Department of Veterans’ Affairs

I quickly confirmed that I was still available

>Hello,
>
>yes, I can confirm my availability. I had already confirmed this appointment.
>
>Thank you,
>
>Greg.

#### 25th of June, 2019 (Day 98)

A week prior to the appointment and I hadn't received any more details about the appointment, like it's location. So I emailed asking for those details.

>Hello Team,
>
>can you please send me the address for this appointment? I haven't received those details yet. Also, is it possible to use normal email instead of this secure email service?
>
>Thank you,
>
>Greg.

#### 4th of July, 2019 (Day 107)

I hadn't received any reply to my previous message and still didn't have a location for the appointment. I reached out again.

>When will I receive the address or any other details about this appointment?

#### 6th of July, 2019 (Day 109)

I hadn't heard from anyone in weeks and had missed my appointment because I was never given the details so I emailed GeneralEnquiries@dva.gov.au & Vijayata again.

>Hello Vijayata,
>
>I replied to both of your confirmation emails regarding my appointment on the 5th of July with Dr ******, in Ottawa, Canada. I sent a third email asking for the address last week as I had no details on the location and I hadn't heard back from you. I still haven't received a response from you or anyone on your team. You made me wait weeks for this appointment and then you didn't even send me the details. I have insomnia and it affects my life. If the DVA is incapable of helping me either because of lack of resources or lack of competence, please let me know so I can get help elsewhere. This level of incompetence unacceptable. Every interaction I have with the Australian DVA is a disappointment.
>
>Please just either help me or admit that you're not going to help me so I don't have to suffer though more months of insomnia.
>
>I have the right to access DVA services in Canada through the Canadian DVA but it needs to be initiated from the Australian DVA. Please please please just do that!
>
>I expect a response within 5 days with a clear resolution.

#### 8th of July, 2019 (Day 111)

Received an apology with no explanation.

>Good Morning Greg,
>
>My apologies for the delayed response.
>
>I am liaising with MLCOA to get another appointment date for you.
>
>I’ll keep you updated.
>
>With Kind Regards,
>
>Vijayata  
>Delegate, Liability  
>Processing Team 2 | Sydney Office | Clients’ Benefits  
>Department of Veterans’ Affairs

#### 9th of July, 2019 (Day 112)

I asked if my messages were being received.

>Hi Vijayata,
>
>I apologise for the tone of my last message but please appreciate my frustration. Having chronic insomnia has put me in a pretty dark place and I either need support or a way out. Can you please acknowledge that you are receiving my messages though this secure mail portal?
>
>Thank you,
>
>Greg.

Vijayata confirmed they were.

>I can confirm that I have received your previous correspondence.
>
>I was, however, unable to respond to you in time in relation to the 05 July 2019 appointment.
>
>My apologies.
>
>As mentioned before, I am arranging a follow-up appointment for you through MLCOA.
>
>With Kind Regards,
>
>Vijayata  
>Delegate, Liability  
>Processing Team 2 | Sydney Office | Clients’ Benefits  
>Department of Veterans’ Affairs

#### 11th of July, 2019 (Day 114)

Received a generic response from the General Enquiries.

>Dear Greg
>
>Thank you for your email.
>
>Your request has been forwarded to the relevant DVA business area for their consideration and response.  Please be aware that while we endeavour to answer your enquiry within 2 to 5 days, further time may be needed to investigate and respond.
>
>If you have any additional questions, feel free to contact us via email or on the phone numbers below to speak directly to a DVA staff member.
>
>Brendan ******,  
>Client Contact Victoria / Client Channels  
>Department of Veterans' Affairs  
>Ph. 1800 555 254  
>All Mail: DVA, GPO Box 9998, BRISBANE QLD 4001 

#### 22nd of July, 2019 (Day 125)

Emailed Vijayata again to find out why I hadn't received any news for a few weeks.

>Why is my appointment taking so long to organize? Why haven't I heard anything from you for weeks? What's going on?

I received a response.

>Good Morning Greg,
>
>My apologies for my delayed response; I had been away sick from work (flu season here in Aus.!)
>
>I have received an email from MLCOA with the earliest available date: Wednesday 4 September 2019 at 10am.
>
>I will email you out the contact details / appointment details when I get these from MLOCA.
>
>With Kind Regards,
>
>Vijayata  
>Delegate, Liability  
>Processing Team 2 | Sydney Office | Clients’ Benefits  
>Department of Veterans’ Affairs

I sympathize with Vijayata getting sick. That's awful, but the DVA should have systems in place to deal with people getting sick. I responded.

>Cheers Vijayata, I hope you're feeling better now. I'm looking forward to my appointment, I'll block it off in my calendar.  
>Thanks again,  
>Greg.

Vijayata responded.

>No worries!
>
>Once again, I do apologise for the delay in arranging this appointment for you.

#### 23rd of July, 2019 (Day 126)

I really want to get some help so I emailed Darren Chester, the Minister responsible for the operations of Veterans' Affairs. I also cc'd GeneralEnquiries@dva.gov.au to give them a heads up.

>Dear Hon Darren Chester MP,
>
>I am an Australian Army veteran that served in Afghanistan & the Solomon Islands. I migrated to Canada in 2012 and have been seeking help from the Australian DVA for my service induced insomnia. It's not going well even though there is a reciprocal relationship between the Australian Department of Veteran Affairs and the Canadian Veterans Affairs.
>
>I have documented my experience so far in a blog post -> https://gregology.net/2019/07/my-experience-with-the-australian-dva/
>
>I would really appreciate if you could assist in any way possible.
>
>Regards,
>
>Greg Clarke.

I received an automated response

>Office of the Hon Darren Chester MP
>
>Minister for Veterans and Defence Personnel  
>Deputy Leader of the House  
>Federal Member for Gippsland  
>
>Thank you for your email to the Office of the Hon Darren Chester MP.
>
>As you would appreciate, Minister Chester receives a considerable amount of correspondence each day and, while all emails will be read and actioned, some may not receive a formal response.
>
>Thank you for taking the time to contact the Minister.

I received a response on the 17th of September (posted below)

#### 29th of July, 2019 (Day 132)

After getting no human response from the Minister for Veterans and Defence Personnel, I decided to reach out to the shadow minister for help.

>Dear Honourable Amanda Rishworth MP,
>
>I am an Australian Army veteran that served in Afghanistan & the Solomon Islands. For the last few months I've been trying to get assistance from the DVA for my service induced insomnia. I'm at my wits’ end trying to get support from the DVA. 
>
>I have documented my communications with the DVA here -> https://gregology.net/2019/07/my-experience-with-the-australian-dva/
>
>It's been over 4 months since I initially reached out for help and I'm no closer to treatment.
>
>I was hoping that you, as a psychologist and the Shadow Minister for Veterans' Affairs could help me. Is there anything you can do to help me get treatment?
>
>I would appreciate any assistance or direction you can give me,
>
>Regards,
>
>Greg.

Clearly my details were out of date and I received this response

>Dear Mr Clarke
>
>Thank you for our email.
>
>Amanda is no longer the Shadow Minister for Veterans’ Affairs.  The new Shadow Minister if Shayne Neumann MP.  I have forwarded your email to his office.
>
>Kind regards – Ethné

I have not received any more responses or follow ups from this email.

#### August 6th, 2019 (Day 140)

I received an address for my assessment appointment!

>Good Morning Greg,
>
>Please find correspondence attached which refers to your upcoming appointment with Dr Ken Suddaby on 04 September 2019.
>
>Please allow between 2-3 hours for the duration of this appointment.
>
>Please bring along two forms of identification to the appointment, with one of those being a photo ID.
>
>With Kind Regards,
>
>Vijayata

Responded

>Confirmed, thank you.


#### August 27th, 2019 (Day 161)

I received another confirmation from a new Senior Delegate

>Dear Mr Clarke,
>
>I am writing to you about your claim for compensation for the conditions ‘Insomnia and Mental Health’.
>
>This is to advise you that this claim has now been reassigned to me and I wanted to confirm that an appointment has been organised for you on 4 September 2019 at 10am as per follows:
>
>Dr Ken ******, (Consultant Psychiatrist)  
>555 Legget Drive  
>Tower A, Suite 304 TTC (The Corporate Centre)  
>Ottawa, ON, K2K 2X3
>
>Please do not hesitate to contact me if you have any enquiries regarding this matter.
>
>Kind Regards
>
>Anita (62210884)  
>Senior Delegate, MRCA Liability  
>Processing Team 2 | Sydney Office | Clients’ Benefits  
>Department of Veterans’ Affairs  
>Phone: 1800 555 254  

Responded

>Hello Anita,
>
>It's a pleasure to meet you. I'm looking forward to the appointment and getting some help.
>
>Cheers,  
>Greg.


#### September 4th, 2019 (Day 169)

Today I met with Dr. Ken ******, for my appointment. Dr. Ken politely declined my request to record the meeting. He would not allow me to take any notes during the meeting as it would be distracting. Dr. Ken also denied my request for a copy of the report or his notes from the session. The assessment was thorough taking 2hrs and 40mins. We discussed my service history, symptoms, previous treatments, life history, & current situation. It was a very draining experience that aggravated some scars.

Dr. Ken asked me why I didn't seek treatment earlier. This was a painful question. Since leaving the Australian army, I have spent thousands of dollars out of my own pocket for service related treatment because the DVA previously refused to help me. Had the DVA given me assistance when I first requested it, my treatment would have begun 4 years ago. Getting this far into the process with the DVA and then being asked by *I* didn't seek treatment earlier was very frustrating.

At the end of the assessment, Dr. Ken made some recommendations for seeking help through OHIP (Ontario Health Insurance Plan) or my work insurance plan. I gathered from his recommendations that my application would be rejected but I appreciated him giving me direction to find help elsewhere.

Dr. Ken will finish his report within a week. The report will then go to MLCOA, an insurance company that helps the DVA assess claims.

I submitted a Freedom of Information request to the DVA so that I can calculate how much this whole experience has cost the DVA in MLCOA fees alone.

> Hello,
>
>I am formally requesting;
>
>1) The names of other organisations used by DVA to write reports (other than MLCOA) and the percentage of the total number of reports written by MLCOA.  
>2) The average amount DVA pays MLCOA doctors for their reports.  
>3) The number of complaints DVA has received from veterans against doctors engaged by DVA for medico-legal service.  
>4) Documents outlining any training material, directions or correspondence provided to MLCOA doctors when dealing with veterans.  
>
>Similar to Disclosure Log Number 87 from https://www.dva.gov.au/about-dva/freedom-information/foi-disclosure-log
>
>Regards,  
>Greg Clarke.

They responded and I should receive a response in the next few months. 

> In accordance with section 15(5)(b) of the FOI Act, the due date for a decision on your request is 7 October 2019.`

#### September 17th, 2019 (Day 182)

Today I received a response from the Minister of Veterans and Defence Personnel, Darren Chester. The message was detailed and they had clearly taken my grievances seriously which I appreciated.

> Dear Mr Clarke
>
> Thank you for your correspondence on the 24 July 2019 to the Minister of Veterans and Defence Personnel, the Hon Darren Chester MP regarding your dealings with the Department of Veterans' Affairs (DVA). The Minister has asked me to respond on his behalf.
>
> I apologise for the miscommunications in arranging your medical appointment. I understand this has been raised with the staff member with advice provided on appropriate future escalation processes. I am further advised that the business area has cross checked all of the escalation processes for persons living overseas in an effort to have future calls routed to the appropriate area.
>
> I understand the mater has now been resolved and you attended an appointment with Dr Suddaby on 4 September 2019. DVA will be able to assess and finalise your claim, once the medical report from Sr Suddaby has been received.
>
> Should you have any further queries about this matter, please contact Karen on 1800 555 xxx and request extension xxxxxx.
>
> Thank you for taking the time to write.
>
> Yours sincerely  
> Robert ******,  
> Chief of Staff

#### September 19th, 2019 (Day 184)

It's been 6 months since I first reached out to the DVA for help. I still have had no treatment provided by the DVA.

#### September 24th, 2019 (Day 189)

Frustrated with waiting, I reached out to my DVA contact. I also requested the report that Dr. Suddaby had prepared on me.

> Hello Anita,
>
> it's been 3 weeks since my assessment and over 6 months since I reached out for help. Is there any word on when the DVA might provide me with treatment? Also, can I please get a copy of Dr. Suddaby's report for my own records.
>
> Thank you,
>
> Greg.

Anita sent me a prompt response which was appreciated

> Dear Mr Clarke,
>
> Thank you for your email.
>
> I have not received the report from Dr Suddaby as yet, I did receive an email from the medico-legal company (MLCOA) yesterday that they are following up on this.
>
> I will finalise your claim as soon as we get the report and, unless indicated otherwise by the doctor, I will forward you a copy of Dr Suddaby’s report with my decision letter.
>
> Please do not hesitate to contact me if you have any further queries regarding this matter.
>
> Kind Regards
>
> Anita (62210884)  
> Senior Delegate, MRCA Liability  
> Processing Team 2 | Sydney Office |Clients’ Benefits  
> Department of Veterans’ Affairs

#### September 30th, 2019 (Day 195)

The DVA responded to my freedom of information request. All my questions were answered with `Following a reasonable search, this information could not be found`. I was hoping to calculate how much this delaying and refusal tactic had cost the Australia tax payers so far. Clearly it's something the DVA is not eager to share.

> Document created in accordance with section 17 of the Freedom of Information Act 1982 (Cth)  
> FOI reference number: 30760
>
> Request details:  
> 1. The names of other organisations used by DVA to write reports (other than MLCOA).
>
> * Australian Medico-Legal Group;
> * E_Reports;
> * IMO Pty Ltd;
> * Medico Legal Opinions;
> * Medico Legal Reporting Services of Australia
> * Red Health Independent Medical Assessments;
> * Specialist Onion Group
> 
> 2. The percentage of the total number of reports written by MLCOA.  
> Following a reasonable search, this information could not be found.
> 
> 3. The average amount DVA pays MLCOA doctors for their reports.  
> Following a reasonable search, this information could not be found.
> 
> 4. The number of complaints DVA has received from veterans against doctors engaged by DVA for medico-legal service.  
> Following a reasonable search, this information could not be found.
> 
> 5. Documents outlining any training material, directions or correspondence provided to MLCOA doctors when dealing with veterans  
> An overview of DVA Compensation and Benefits was presented to MLCOA Doctors in May 2019. See Document 2.
> 
> 6. The average amount DVA pays MLCOA doctors for their reports from Canada.  
> Following a reasonable search, this information could not be found.
> 
> 7. The average amount DVA pays MLCOA doctors for their reports from outside of Australia...”  
> Following a reasonable search, this information could not be found.

#### October 6th, 2019 (Day 201)

As my previous freedom of information requests were rejected on the ground that `Following a reasonable search, this information could not be found`, I decided to make it really simple for the DVA.

> Hello,
>
> I arm formally requesting;  
> 1) The amount paid to MLCOA for the assessment of myself (Gregory Clarke).  
> Regards,  
> Greg Clarke.

#### October 7th, 2019 (Day 202)

I received a prompt response with clarifying questions which I responded to.

> Good Morning
>
> Thank you for the below email.
>
> In order for us to action your request we need more information in order to identify you on our database.
>
> Could you please provide your date of birth and your Veterans Affair file number please.
>
> Kind Regards  
> Arthur ******,

#### October 9th, 2019 (Day 204)

My claim has been accepted

> Dear Mr Clarke,
>
> I am writing to you about your claim for compensation for the conditions ‘Insomnia and Mental Health’.
>
> I received the report from Dr Suddaby yesterday and I have now finalised this claim and my decision letter is attached for your perusal.
>
> As requested by you, I have also attached Dr Suddaby’s report for your records.
>
> Please do not hesitate to contact me if you have any queries regarding this matter.
>
> Kind Regards
> 
> Anita (62210884)  
> Senior Delegate, MRCA Liability  
> Processing Team 2 | Sydney Office |Clients’ Benefits  
> Department of Veterans’ Affairs  
> Phone: 1800 555 254  
> Email: rcg.sydney.liability@dva.gov.au

tl;dr of attached letter

> DECISION SUMMARY  
> I have accepted your claim for:  
> * Specified Trauma or Stressor Related Disorder with effect from 30 April 2011  
> * Adjustment Disorder with Mixed Mood with effect from 30 April 2011  
> *  Chronic Insomnia Disorder with effect from 30 April 2011  

Hopefully I can start treatment soon

#### October 10th, 2019 (Day 205)

Having my claim accepted was validating and a huge relief. It may still be months before I start treatment but this was the first time I felt like the DVA were actually supporting me. I responded to Anita.

> Thank you Anita, this is great news. I'll wait to hear from the delegate and organize some rehabilitation locally.  
> Thank you again for your help,  
> Greg

#### October 14th, 2019 (Day 209)

Mitch from the DVA reached out offering incapacity payments. I declined the offer, all I want is treatment. But it was nice knowing that they were taking my condition seriously.

#### October 15th, 2019 (Day 210)

Rochelle from Incite Solutions Group reached out or organise an initial assessment. She promptly setup a Skype chat for for Thursday night my time.

#### October 17th, 2019 (Day 212)

A weight that had been centered in my chest has disappeared. After an hour long chat with Giorgia & Rochelle from Incite Solutions Group, I finally feel like I will receive treatment soon. It's been a long journey. I received an email from Giorgia requesting some additional paperwork.

#### October 22nd, 2019 (Day 217)

I returned 3 of the 4 documents, `Return to Work Rehabilitation Rights and Obligations`, `Life Satisfaction Indicators`, & `CONSENT TO RELEASE AND OBTAIN INFORMATION`. The 4th document required a doctor to fill it out. I booked an appointment to see my doctor at the earliest appointment, the 12th of November.

#### November 6th, 2019 (Day 232)

I received yet another rejection for my Freedom of Information request.

> Decision  
> I have made a decision to refuse one document in full in accordance with section 47G of the FOI Act.

#### November 12th, 2019 (Day 238)

I visited my doctor and she helpfully filled out the confusing document `Medical Certification - Ability to Work`. To be clear, I have never claimed that I am unable to work. I want to get support so that I can remain employed. As soon as I returned home I scanned the document and sent them off to Giorgia.

#### November 13th, 2019 (Day 239)

It turns out that my doctor didn't tick a box on the confusing `Medical Certification - Ability to Work` document. That meant I have to return to my doctor to get the box ticked and initialled.

> Hi Greg,
>
> Thank you for sending that through. I have reviewed the Medical Certification and noticed that Dr S has crossed out the vocational rehabilitation box, which would indicate you are not fit to engage in vocational rehabilitation with Incite Solutions.
> 
> In order you to participate in vocational rehabilitation services with us and for us to provide support, Dr S would need to tick you have some capacity in
> ‘Part B – Rehabilitation – Question 7’ i.e. at least 1-2 hours per week to come on to Programme with us. Apologies if that was not clear, it mentions activities such as online learning and work-trials, which are not relevant for you as you are currently working full-time. However, the Rehab plan we came up with for the medical, psychosocial and vocational goals are what we would be supporting with from a vocational rehabilitation perspective and from an employment perspective, we would be supporting with employment monitoring (i.e. your vocational goal to lessen occasions of lateness to work).
> 
> Would you be able to book another appointment with Dr S or alternatively call the practice and ask her to make an amendment and initial this with her signature on the previously completed Med Cert – indicating you have some capacity for vocational rehabilitation in Part B – Rehabilitation – Question 7?
> 
> Could you please let me know when this may be possible, as I need to update DVA and request an extension until we are able to obtain the updated Medical Certificate.
> 
> Let me know if you any of that doesn’t make sense.
> 
> Warm regards, 
> Giorgia

I really thought that I was past DVA bureaucracy and hoop jumping now that I was dealing directly with Incite Solutions. Unfortunately I was wrong. I have booked a the earliest available follow up appointment with my doctor on the 5th of December.

> I've booked another appointment with my doctor on the 5th of December. In the meantime, can I please get some help.

#### November 14th, 2019 (Day 240)

Giorgia tried to push back on the DVA and got nowhere. She offered to chase the paperwork directly to save me another visit to the doctor. After 8 months of getting nowhere, I was really running out of energy dealing with the DVA so I appreciated Giorgia's efforts.

> Hi Greg,
> 
> I have spoken to DVA and unfortunately they require an updated medical certificate that reflects you are fit for vocational rehabilitation before we can submit the Initial Assessment and commence you on program. I apologise for the trouble. I am going to organise for a fax to be sent to Dr S as she only needs to amend the ‘Part B – Rehabilitation – Question 7’ section and I am hopeful she may be able to complete this before December 5th so you don’t have to wait for another appointment. Let me know your thoughts and whether you think she would respond to the fax from us?
> 
> Warm regards, 
> Giorgia

#### November 15th, 2019 (Day 241)

I responded thanking Giorgia

> Thanks Giorgia, I appreciate your help. I don't blame you, I know this is dva bureaucracy. And yes please, reach out to Dr S directly, that would be great.
> 
> Thanks again, and have a great weekend, 
> Greg.

Giorgia again apologised for the DVA making me jump though hoops

> I really do apologise for the run around Greg and appreciate your understanding. I will give you an update as soon as I get something back from Dr S. Once we have everything submitted to DVA, we can hopefully organise a time to Skype again so we can discuss next steps for the program.
> 
> Hope you have a great weekend as well.
> 
> Warm regards, 
> Giorgia

#### Next steps

For the most part, the Australian DVA staff are great however the organisation has failed me and is failing many others. If I had known that it was going to take 6 months to be assessed for treatment, I would have organised help elsewhere and would be months into my treatment. It pains me knowing that my mates with more severe issues are having to deal with this level of service. Asking for help is the hardest step and the Australian DVA's promise of help with slow or no follow through is unbearable. Unfortunately I have veteran friends that have committed suicide. I honestly believe that they might still be here if the DVA didn't exist.

### What I want

 - The Australian DVA to be dissolved so that diggers can get help instead of jumping through hoops

  OR

 - Accurate assessments on treatment schedules so that veterans can make informed decisions as to whether they should proceed with the DVA or get help elsewhere
 - Better training of DVA staff
 - Improved communications between DVA staff and veterans
 - Improved communications between DVA staff and their international counterparts (inaccessible reciprocal services are not reciprocal)
 - And finally, **treatment for my service induced insomnia**

<!--
  # Python days calculation
  from datetime import date
  date(2019,11,6) - date(2019,3,19)
-->
