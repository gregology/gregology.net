---
title: replacements have arrived
author: Greg
layout: post
permalink: /2009/04/replacements-have-arrived/
comments: True
categories:
  - Army
  - News
---
This is a big day for me because it secures my return home :) It means that someone is here to do my job so there is no reason for me to be here. I haven&#8217;t met our replacements yet and I&#8217;m keen to get them trained up so I can hit the piss tomorrow night.

Mark and I moved into our new room. It&#8217;s pretty funny, they always keep us near to each other. We sit next to each other on planes and this is the 7th room I&#8217;ve shared with him. As shift working diggers we scored an air conditioned room as apposed to tent lines but we have to share with Sargents which means we need to behave. It sound like they&#8217;re having a party down at the tent lines, but at least we&#8217;re acclimatising to the cold weather. In four days we&#8217;re going to be in Autumn :(

The outgoing rotation smell excited which contrasts the nervous odor of the newbies. They are very keen and it&#8217;s interesting to see how the two groups interact. Two diggers just ran past me in their full army costume, they are very keen. I&#8217;m outside on a breeze way taking advantage of one of my last working lazy days. Afternoon shifts mean I don&#8217;t start until four.

My post deployment psych interview was on Monday. I passed. They gave us some pointers on fitting back into non military society. It will be hard, at the moment I find it so difficult not to punch the slow walking civilian staff in the back of the head when they are strolling down the paths to the mess or where every those snails go to eat. It is hard to imagine a whole street full of civilians. I didn&#8217;t mention this to the psych, but he did say he thought I was holding something back.