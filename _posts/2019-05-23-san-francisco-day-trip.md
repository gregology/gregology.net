---
title: "San Francisco Day Trip"
author: Greg
layout: post
permalink: /2019/05/san-francisco-day-trip/
date: 2019-05-23 21:38:53 -0700
comments: True
licence: Creative Commons
categories:
  - travel
tags:
  - San Francisco
images:
  - image: /wp-content/uploads/2019/05/san-francisco-trip/IMG_20190523_122734.jpg
    description: I had an interview in San Francisco so I took a day to explore
  - image: /wp-content/uploads/2019/05/san-francisco-trip/IMG_20190523_112242.jpg
    description: Every boat in this marina seemed to have the same height mast
  - image: /wp-content/uploads/2019/05/san-francisco-trip/IMG_20190523_114829.jpg
    description: Found what I initially thought was the Golden Gate Bridge
  - image: /wp-content/uploads/2019/05/san-francisco-trip/IMG_20190523_123240.jpg
    description: I found a seal
  - image: /wp-content/uploads/2019/05/san-francisco-trip/IMG_20190523_123431.jpg
    description: Then I found a load more
  - image: /wp-content/uploads/2019/05/san-francisco-trip/IMG_20190523_125003.jpg
    description: I found USS Pampanito
  - image: /wp-content/uploads/2019/05/san-francisco-trip/IMG_20190523_124632.jpg
    description: Complete with a broom from her mast, indicating a clean sweep of the seas
  - image: /wp-content/uploads/2019/05/san-francisco-trip/IMG_20190523_125644.jpg
    description: I ate seafood in Fisherman's Wharf
  - image: /wp-content/uploads/2019/05/san-francisco-trip/IMG_20190523_131903.jpg
    description: Discovered a fleet of cute fishing boats
  - image: /wp-content/uploads/2019/05/san-francisco-trip/IMG_20190523_132857.jpg
    description: Found a tall ship
  - image: /wp-content/uploads/2019/05/san-francisco-trip/IMG_20190523_133533.jpg
    description: Found a really steep hill
  - image: /wp-content/uploads/2019/05/san-francisco-trip/IMG_20190523_133606.jpg
    description: Waited for a car chase scene
  - image: /wp-content/uploads/2019/05/san-francisco-trip/IMG_20190523_140914.jpg
    description: Climbed a steep hill
  - image: /wp-content/uploads/2019/05/san-francisco-trip/IMG_20190523_141022.jpg
    description: Got to the top
  - image: /wp-content/uploads/2019/05/san-francisco-trip/IMG_20190523_141149.jpg
    description: Found a wiggly road
  - image: /wp-content/uploads/2019/05/san-francisco-trip/IMG_20190523_141448.jpg
    description: Saw cars wiggle
  - image: /wp-content/uploads/2019/05/san-francisco-trip/IMG_20190523_134329.jpg
    description: Visited the sea side
  - image: /wp-content/uploads/2019/05/san-francisco-trip/IMG_20190523_134555.jpg
    description: Found a gumtree
  - image: /wp-content/uploads/2019/05/san-francisco-trip/IMG_20190523_143528.jpg
    description: Saw some awesome looking trams
  - image: /wp-content/uploads/2019/05/san-francisco-trip/IMG_20190523_161526.jpg
    description: Met up with an acro friend (Debby)
  - image: /wp-content/uploads/2019/05/san-francisco-trip/IMG_20190523_174157.jpg
    description: Explored Chinatown looking for an arch
  - image: /wp-content/uploads/2019/05/san-francisco-trip/IMG_20190523_174511.jpg
    description: Found an arch
  - image: /wp-content/uploads/2019/05/san-francisco-trip/IMG_20190523_174857.jpg
    description: Saw one of those cool firetrucks with a rear steerer
  - image: /wp-content/uploads/2019/05/san-francisco-trip/IMG_20190523_175135.jpg
    description: Saw some fun buildings
  - image: /wp-content/uploads/2019/05/san-francisco-trip/IMG_20190523_180549.jpg
    description: Found fun art
  - image: /wp-content/uploads/2019/05/san-francisco-trip/IMG_20190523_182819.jpg
    description: Met a mean robot that hassles homeless people
  - image: /wp-content/uploads/2019/05/san-francisco-trip/IMG_20190523_184646.jpg
    description: Found it later hassling a pile of clothes
---

{% include gallery.html %}
