---
title: 'I&#8217;m in Newcastle'
author: Greg
layout: post
permalink: /2007/03/im-in-newcastle/
comments: True
categories:
  - Uncategorized
---
I&#8217;ve left Sydney and am in Newcastle with my sister and new brother in law. My Dad drove me up. I&#8217;m no longer a postie and I&#8217;m excited about my travels. I don&#8217;t have to get up at 5 tomorrow so I&#8217;m quite happy. My going away party was on friday and I&#8217;ve had a few beers now so I&#8217;m a little tipsy. I&#8217;m also a little sad about leaving everyone. I&#8217;ll keep you in touch.