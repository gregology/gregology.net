---
title: Ruby Tuesday at Shopify notes
author: Greg
layout: post
permalink: /2012/11/ruby-tuesday-at-shopify-notes/
comments: True
categories:
  - Geek
tags:
  - geek
  - ruby
  - ruby tuesday
  - shopify
---
Tonight I&#8217;m playing with the Ottawian Ruby crowd at Ruby Tuesday hosted by Shopify. My notes are limited as there was free beer.

First talk &#8211; Internationalization by @J3

Internationalization &#8211; done once  
localization &#8211; done at each location

You should internationalization your code even if your only deploying to one locale

Locales are a mix of geography and language

Don&#8217;t write magic in your code, magic in programming is processes that you haven&#8217;t taken the time to understand yet.

Copy should be it&#8217;s own set. Text &#8220;Article was created&#8221; = Concept &#8220;successful article created&#8221;

&#8220;A method longer than 8 lines has something wrong&#8221;