---
title: Radio checks and sleeping
author: Greg
layout: post
permalink: /2009/02/radio-checks-and-sleeping/
comments: True
categories:
  - Army
  - Work
---
There coming to pick us up tomorrow. I was hoping we&#8217;d leave today because I really needed to go to the toilet and didn&#8217;t like the idea of squatting over a soggy hole in tall grass but I wasn&#8217;t so lucky and had an awful experience. On the up side another night here probably means more pipping hot sweat potatoes from the locals and another visit to the river for a swim. But now that live gotten use to the insects, heat and mud I&#8217;m actually enjoying myself. I&#8217;ve slowed to the pace of the locals. I just lie on my reed bed studying the insects and the occasional radio check. The locals come and join us, they just sit next to us for half the day. I&#8217;m not sure how the economy works in these parts but it makes the locals very time rich. I guess the jungle must provide. Everyone else is asleep, a pig is investigating us and the local children from the school are watching. I think I&#8217;ll have another sleep then a swim in the river.